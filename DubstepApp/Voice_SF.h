//
//  Voice_SF.h
//  DubstepApp
//
//  Created by Mike Li on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Voice.h"
#import "SoundFile.h"

@interface Voice_SF : Voice {
  SoundFile* soundfile; 
}

- (id)init:(BOOL)repeat withFile:(NSString*)file;
-(void)changeSoundFile:(NSString*)file;
-(void)fillSampleBuffer:(Float64*)buffer withSamples:(UInt32)num_samples;
-(void)resetVoice;

@end
