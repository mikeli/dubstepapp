//
//  AQPlayer_SF.h
//  MySecondApp
//
//  Created by Kojiro Umezaki on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AQPlayer.h"
#import "SoundFile.h"
#import "Voice_SF.h"

@interface AQPlayer_SF : AQPlayer {
  Voice_SF* voices_SF[kNumberVoices];
}
-(id)init;
-(void)setVoices:(UInt32)voice_number withRepeat:(BOOL)repeat withFile:(NSString*)file;
-(void)changeSoundFile:(UInt32)voice_number withFile:(NSString*)file;
-(void)fillAudioBuffer:(Float64*)buffer withSamples:(UInt32)num_samples;
-(void)resetVoice:(UInt32)voice_number;
-(void)voiceToggle:(BOOL)on withPosition:(UInt16)pos;
-(void)voicesOff;
-(void)setVoicesToHalfAmp;
-(void)setVoicesToFullAmp;
-(void)setBassVoiceTo:(Float64)amp;
-(void)beatToggle;
-(void)filterFreq:(Float64)freq;
@end
