//
//  AQPlayer.h
//  MInC
//
//  Created by Kojiro Umezaki on 4/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <Foundation/Foundation.h>

#import "Voice.h"
#import "Effect.h"

/* number of buffers used by system */
#define kNumberBuffers	4

/* number of voices */
#define kNumberVoices   9

/* number of effects */
#define kNumberEffects  1

/* sample rate */
#define kSR				22050.

@interface AQPlayer : NSObject {

	AudioQueueRef				queue;
	AudioQueueBufferRef			buffers[kNumberBuffers];
	AudioStreamBasicDescription	dataFormat;
    
    Voice* voices[kNumberVoices];
    Effect* effects[kNumberEffects];
}

-(void)setup;

-(OSStatus)start;
-(OSStatus)stop;

-(void)fillAudioBuffer:(Float64*)buffer withSamples:(UInt32)num_samples;

-(void)voiceToggle:(UInt16)pos;
-(void)reportElapsedTime:(Float64)elapsed_time;
@end
