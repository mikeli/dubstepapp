//
//  main.m
//  DubstepApp
//
//  Created by Mike Li on 10/6/12.
//  Copyright (c) 2012 Mike Li. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
