//
//  Voice_SF.m
//  DubstepApp
//
//  Created by Mike Li on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Voice_SF.h"

@implementation Voice_SF

- (id)init:(BOOL)repeat withFile:(NSString*)file
{
  self = [super init];
  soundfile = [[SoundFile alloc] init:file];
  soundfile.repeat = repeat;
  return self;
}

-(void)changeSoundFile:(NSString*)file
{
  [soundfile close];
  soundfile = [[SoundFile alloc] init:file];
}

-(void)fillSampleBuffer:(Float64*)buffer withSamples:(UInt32)num_samples
{
  [soundfile fillSampleBuffer:buffer withSamples:num_samples withVoice:self withAmp:amp];
}

-(void)resetVoice
{
  soundfile.filePos = 0;
}

@end
