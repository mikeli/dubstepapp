//
//  AQPlayer_SF.m
//  MySecondApp
//
//  Created by Kojiro Umezaki on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AQPlayer_SF.h"
#import "Effect_Biquad.h"

@implementation AQPlayer_SF

-(id)init
{
    self = [super init];
    effects[0] = [[Effect_Biquad alloc] init];
    [(Effect_Biquad*)effects[0] biQuad_set:LPF withGain:0. withFrequency:5000 withSamplingRate:kSR withBandwidth:1.0];
    return self;
}

-(void)setVoices:(UInt32)voice_number withRepeat:(BOOL)repeat withFile:(NSString*)file
{
  voices_SF[voice_number] = [[Voice_SF alloc] init:repeat withFile:file];
}

-(void)changeSoundFile:(UInt32)voice_number withFile:(NSString*)file
{
  [voices_SF[voice_number] changeSoundFile:file];
}

-(void)resetVoice:(UInt32)voice_number
{
    [voices_SF[voice_number] resetVoice];
}

-(void)fillAudioBuffer:(Float64*)buffer withSamples:(UInt32)num_samples
{     
    [voices_SF[kNumberVoices-1] fillSampleBuffer:buffer withSamples:num_samples];
//    [effects[0] process:buffer:num_samples];
    for (SInt32 i = 0; i < kNumberVoices-1; i++) {
        if (voices_SF[i] != nil)
            [voices_SF[i] fillSampleBuffer:buffer withSamples:num_samples];
    }
    
}

-(void)voiceToggle:(BOOL)on withPosition:(UInt16)pos
{
    if (on) {
        [voices_SF[pos] on];
    } else {
        [voices_SF[pos] off];
    }
}

-(void)voicesOff{
    for(SInt32 i = 1; i < kNumberVoices; i++) {
        [voices_SF[i] off];
        [voices_SF[i] resetVoice];
    }
}

-(void)setVoicesToHalfAmp
{
    for (SInt32 i = 0; i < kNumberVoices-1; i++) {
        if ([voices_SF[i] isOn]) {
            voices_SF[i].amp = 1.0/10.;
        }
    }
}

-(void)setVoicesToFullAmp
{
    for (SInt32 i = 0; i < kNumberVoices-1; i++) {
        if ([voices_SF[i] isOn]) {
            voices_SF[i].amp = 1.0/5.;
        }
    }
}

-(void)setBassVoiceTo:(Float64)amp
{
    voices_SF[kNumberVoices-1].amp = amp;
}

-(void)beatToggle
{
    if ([voices_SF[0] isOn]) {
        [voices_SF[0] off];
        [voices_SF[0] resetVoice];
    } else {
        [voices_SF[0] on];
    }
}

-(void)filterFreq:(Float64)freq
{
    [((Effect_Biquad*)effects[0]) biQuad_set:LPF withGain:0. withFrequency:freq withSamplingRate:kSR withBandwidth:1.0];
}

@end
