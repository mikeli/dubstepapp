//
//  AppDelegate.h
//  DubstepApp
//
//  Created by Mike Li on 10/6/12.
//  Copyright (c) 2012 Mike Li. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
