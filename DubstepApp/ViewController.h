//
//  ViewController.h
//  DubstepApp
//
//  Created by Mike Li on 10/6/12.
//  Copyright (c) 2012 Mike Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "AQPlayer_SF.h"

@interface ViewController : UIViewController {
    // the almighty AQPlayer
    AQPlayer_SF*   aqp;
  
    // toolbar
    IBOutlet UIToolbar *toolbar;
    
    // thumb button
    IBOutlet UIButton *thumbButton;
    // start/stop button
    IBOutlet UIBarButtonItem *startStopButton;
    // right/left-handed orientation button
    IBOutlet UIBarButtonItem *orientationButton;
    // Note buttons
    IBOutlet UIButton *note1;
    IBOutlet UIButton *note2;
    IBOutlet UIButton *note3;
    IBOutlet UIButton *note4;
    IBOutlet NSLayoutConstraint *toolbarHeight;
    IBOutlet NSLayoutConstraint *toolbarBottom;
    IBOutlet NSLayoutConstraint *thumbButtonLeading;
    IBOutlet NSLayoutConstraint *note1Right;
    IBOutlet NSLayoutConstraint *note2Right;
    IBOutlet NSLayoutConstraint *note4Right;
    IBOutlet NSLayoutConstraint *bassDropLabelLeading;
    IBOutlet NSLayoutConstraint *chordLabelLeading;
  
  
    // label for bass drop
    IBOutlet UILabel *bassDropLabel;
    // label for chords
    IBOutlet UILabel *chordLabel;
    
    // toolbar transparency toggle
    BOOL toggleToolbarAlpha;
    // layout of the buttons for right/left handed orientation toggle
    BOOL toggleLayout;
    Float64 secPerBeat;
    UInt32 currentBeat;
    NSTimer* noteTimer;
    BOOL beatToggle;
    BOOL PlayBass;
}

// toggle the toolbar opacity
- (IBAction)thumbButtonAction:(id)sender;
// change the layout of the buttons for right or left handed people
- (IBAction)orientationButtonSelected:(id)sender;
// play or stop the beat
- (IBAction)startButtonSelected:(id)sender;

// play and reset the notes
- (IBAction)note1:(id)sender;
- (IBAction)note2:(id)sender;
- (IBAction)note3:(id)sender;
- (IBAction)note4:(id)sender;

// checks to see if it should play the bass drop
-(void)PlayBass;

@end
