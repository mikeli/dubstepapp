//
//  ViewController.m
//  DubstepApp
//
//  Created by Mike Li on 10/6/12.
//  Copyright (c) 2012 Mike Li. All rights reserved.
//

#import "ViewController.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface ViewController ()

@end

@implementation ViewController

- (BOOL)prefersStatusBarHidden {
  if (SYSTEM_VERSION_LESS_THAN(@"7.0") && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    return YES;
  } else {
    return NO;
  }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // for iOS 6
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
      [thumbButton setBackgroundColor:nil];
      [note1 setBackgroundColor:nil];
      [note2 setBackgroundColor:nil];
      [note3 setBackgroundColor:nil];
      [note4 setBackgroundColor:nil];
      [orientationButton setTintColor:nil];
      [startStopButton setTintColor:nil];
      [toolbarHeight setConstant:44];
      if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [toolbarBottom setConstant:64];
      }
    } else {
        [thumbButton.layer setCornerRadius:10];
        [note1.layer setCornerRadius:10];
        [note2.layer setCornerRadius:10];
        [note3.layer setCornerRadius:10];
        [note4.layer setCornerRadius:10];
    }
    // setup AQPlayer
    aqp = [[AQPlayer_SF alloc] init];
  
    // set all the voices for the notes and beat button
    [aqp setVoices:0 withRepeat:TRUE withFile:@"DubstepTune"];
    [aqp setVoices:1 withRepeat:FALSE withFile:@"Note A5"];
    [aqp setVoices:2 withRepeat:FALSE withFile:@"Note B5"];
    [aqp setVoices:3 withRepeat:FALSE withFile:@"Note C5"];
    [aqp setVoices:4 withRepeat:FALSE withFile:@"Note D5"];
    [aqp setVoices:5 withRepeat:FALSE withFile:@"Note E5"];
    [aqp setVoices:6 withRepeat:FALSE withFile:@"Note F5"];
    [aqp setVoices:7 withRepeat:FALSE withFile:@"Note E4"];
    [aqp setVoices:kNumberVoices-1 withRepeat:TRUE withFile:@"Bass"];
    
	  // set the toggle values
    toggleToolbarAlpha = TRUE;
    toggleLayout = TRUE;
    beatToggle = TRUE;
    PlayBass = FALSE;
    
    // calculate beats for later
    secPerBeat = 60./130;
    currentBeat = 0;
    
    // change the status bar to black
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self becomeFirstResponder];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (IBAction)thumbButtonAction:(id)sender {
    // fade animate the view out of view by affecting its alpha
	[UIView animateWithDuration:0.5 animations:^{
        if (toggleToolbarAlpha) {
            toolbar.alpha = 0.0;
            [thumbButton setTitle: @"Toolbar (show)" forState:UIControlStateNormal];
        } else {
            toolbar.alpha = 1.0;
            [thumbButton setTitle: @"Toolbar (hide)" forState:UIControlStateNormal];
        }
        toggleToolbarAlpha = !toggleToolbarAlpha;
  }];
}

- (IBAction)orientationButtonSelected:(id)sender {
    if (toggleLayout) {
        [orientationButton setTitle:@"Right"];
    } else {
        [orientationButton setTitle:@"Left"];
    }
    [self.view layoutIfNeeded];
    if (toggleLayout) {
        // thumb button
        [self.view removeConstraint:thumbButtonLeading];
        thumbButtonLeading = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:thumbButton attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
        [self.view addConstraint:thumbButtonLeading];
        // note 1 button
        [self.view removeConstraint:note1Right];
        note1Right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:note1 attribute:NSLayoutAttributeLeading multiplier:1 constant:-10];
        [self.view addConstraint:note1Right];
        // note 2 button
        [self.view removeConstraint:note2Right];
        note2Right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:note2 attribute:NSLayoutAttributeLeading multiplier:1 constant:-10];
        [self.view addConstraint:note2Right];
        // note 4 button
        [self.view removeConstraint:note4Right];
        note4Right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:note4 attribute:NSLayoutAttributeLeading multiplier:1 constant:-10];
        [self.view addConstraint:note4Right];
        // bass drop label
        [self.view removeConstraint:bassDropLabelLeading];
        bassDropLabelLeading = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:bassDropLabel attribute:NSLayoutAttributeTrailing multiplier:1 constant:20];
        [self.view addConstraint:bassDropLabelLeading];
        // chord label
        [self.view removeConstraint:chordLabelLeading];
        chordLabelLeading = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:chordLabel attribute:NSLayoutAttributeTrailing multiplier:1 constant:20];
        [self.view addConstraint:chordLabelLeading];
        [bassDropLabel setTextAlignment:NSTextAlignmentRight];
        [chordLabel setTextAlignment:NSTextAlignmentRight];
    } else {
        // thumb button
        [self.view removeConstraint:thumbButtonLeading];
        thumbButtonLeading = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:thumbButton attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
        [self.view addConstraint:thumbButtonLeading];
        // note 1 button
        [self.view removeConstraint:note1Right];
        note1Right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:note1 attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
        [self.view addConstraint:note1Right];
        // note 2 button
        [self.view removeConstraint:note2Right];
        note2Right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:note2 attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
        [self.view addConstraint:note2Right];
        // note 4 button
        [self.view removeConstraint:note4Right];
        note4Right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:note4 attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
        [self.view addConstraint:note4Right];
        // bass drop label
        [self.view removeConstraint:bassDropLabelLeading];
        bassDropLabelLeading = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:bassDropLabel attribute:NSLayoutAttributeLeading multiplier:1 constant:-20];
        [self.view addConstraint:bassDropLabelLeading];
        // chord label
        [self.view removeConstraint:chordLabelLeading];
        chordLabelLeading = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:chordLabel attribute:NSLayoutAttributeLeading multiplier:1 constant:-20];
        [self.view addConstraint:chordLabelLeading];
        [bassDropLabel setTextAlignment:NSTextAlignmentLeft];
        [chordLabel setTextAlignment:NSTextAlignmentLeft];
    }
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
    toggleLayout = !toggleLayout;
}

- (IBAction)startButtonSelected:(id)sender {
    [aqp beatToggle];
    if (beatToggle) {
        [startStopButton setTitle:@"Stop"];
        noteTimer = [NSTimer timerWithTimeInterval:(NSTimeInterval)secPerBeat target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:noteTimer forMode:NSDefaultRunLoopMode];
    } else {
        [startStopButton setTitle:@"Start"];
        [noteTimer invalidate];
        [self.view setBackgroundColor:[UIColor colorWithRed:0 green:0.27 blue:0.83 alpha:1]];
        [note1 setTitle:@"A" forState:UIControlStateNormal];
        [note2 setTitle:@"D" forState:UIControlStateNormal];
        [note3 setTitle:@"E" forState:UIControlStateNormal];
        [note4 setTitle:@"F" forState:UIControlStateNormal];
        [chordLabel setText:@"Chord: Am"];
        // reset the bass drop label
        [bassDropLabel setText:@"No Bass Drop"];
        [chordLabel setTextColor:[UIColor whiteColor]];
        [bassDropLabel setTextColor:[UIColor whiteColor]];
        [aqp voiceToggle:FALSE withPosition:kNumberVoices-1];
        [aqp resetVoice:kNumberVoices-1];
        PlayBass = FALSE;
        currentBeat = 0;
    }
    beatToggle = !beatToggle;
}

- (IBAction)note1:(id)sender {
    if(currentBeat < 4){
        [aqp resetVoice:1];
        [aqp voiceToggle:TRUE withPosition:1];
    } else if(currentBeat < 6)
    {
        [aqp resetVoice:3];
        [aqp voiceToggle:TRUE withPosition:3];
    } else
    {
        [aqp resetVoice:7];
        [aqp voiceToggle:TRUE withPosition:7];
    }
}

- (IBAction)note2:(id)sender {
    if(currentBeat < 6)
    {
        [aqp resetVoice:4];
        [aqp voiceToggle:TRUE withPosition:4];
    } else
    {
        [aqp resetVoice:2];
        [aqp voiceToggle:TRUE withPosition:2];
    }
}

- (IBAction)note3:(id)sender {
    if(currentBeat < 6)
    {
        [aqp resetVoice:5];
        [aqp voiceToggle:TRUE withPosition:5];
    } else
    {
        [aqp resetVoice:3];
        [aqp voiceToggle:TRUE withPosition:3];
    }
}

- (IBAction)note4:(id)sender {
    if(currentBeat < 6)
    {
        [aqp resetVoice:6];
        [aqp voiceToggle:TRUE withPosition:6];
    } else
    {
        [aqp resetVoice:4];
        [aqp voiceToggle:TRUE withPosition:4];
    }
}

-(void)updateNotes
{
    if (PlayBass) {
        [aqp setVoicesToHalfAmp];
    }
    // fade animate the view out of view by affecting its alpha
	[UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        switch (currentBeat) {
            case 0:
                [note1 setTitle:@"A" forState:UIControlStateNormal];
                [note2 setTitle:@"D" forState:UIControlStateNormal];
                [note3 setTitle:@"E" forState:UIControlStateNormal];
                [note4 setTitle:@"F" forState:UIControlStateNormal];
                [chordLabel setText:@"Chord: Am"];
                [self PlayBass];
                [self.view setBackgroundColor:[UIColor colorWithRed:0 green:0.27 blue:0.83 alpha:1]];
                break;
            case 2:
                [chordLabel setText:@"Chord: C"];
                [self.view setBackgroundColor:[UIColor colorWithRed:0.22 green:0.82 blue:0.43 alpha:1]];
                break;
            case 4:
                [note1 setTitle:@"C" forState:UIControlStateNormal];
                [chordLabel setText:@"Chord: Dm"];
                [chordLabel setTextColor:[UIColor blackColor]];
                [bassDropLabel setTextColor:[UIColor blackColor]];
                [self.view setBackgroundColor:[UIColor colorWithRed:0.92 green:0.47 blue:0.21 alpha:1]];
                break;
            case 6:
                [note1 setTitle:@"E" forState:UIControlStateNormal];
                [note2 setTitle:@"B" forState:UIControlStateNormal];
                [note3 setTitle:@"C" forState:UIControlStateNormal];
                [note4 setTitle:@"D" forState:UIControlStateNormal];
                [chordLabel setText:@"Chord: Em7"];
                [chordLabel setTextColor:[UIColor whiteColor]];
                [bassDropLabel setTextColor:[UIColor whiteColor]];
                [self.view setBackgroundColor:[UIColor colorWithRed:0.96 green:0.2 blue:0.17 alpha:1]];
                break;
            default:
                break;
        }
    } completion:nil];
}

- (void)timerFired:(NSTimer *)timer
{
	// time has passed, hide the HoverView
    currentBeat = (currentBeat+1)%8;
    [self updateNotes];
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    // Do your thing after shaking device
    PlayBass = TRUE;
    [bassDropLabel setText:@"Bass Drop Active"];
}

-(void)PlayBass {
    if (PlayBass) {
        [aqp voiceToggle:TRUE withPosition:kNumberVoices-1];
        [aqp setBassVoiceTo:0.5];
    } else {
        [aqp voiceToggle:FALSE withPosition:kNumberVoices-1];
        [bassDropLabel setText:@"No Bass Drop"];
    }
    PlayBass = FALSE;
}
@end
