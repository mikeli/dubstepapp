//
//  SoundFile.m
//  MInC
//
//  Created by Kojiro Umezaki on 6/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SoundFile.h"
#import "Voice_SF.h"

@implementation SoundFile

@synthesize filePos, repeat;

-(id)init:(NSString*)file
{
	self = [super init];
	
    /* get a path to the sound file */
    /* note that the file name and file extension are set here */
	CFURLRef mSoundFileURLRef = CFBundleCopyResourceURL(CFBundleGetMainBundle(),(__bridge CFStringRef)file,CFSTR("aif"),NULL);

	/* open the file and get the fileID */
	OSStatus result = noErr;
	result = AudioFileOpenURL(mSoundFileURLRef,kAudioFileReadPermission,0,&fileID);
	if (result != noErr)
		NSLog(@"AudioFileOpenURL exception %d",(int)result);
	return self;
}

-(void)close
{
	/* close the file */
	OSStatus result = noErr;
	result = AudioFileClose(fileID);
	if (result != noErr)
		NSLog(@"AudioFileClose %d",(int)result);
}


-(void)fillSampleBuffer:(Float64*)buffer withSamples:(UInt32)num_buf_samples withVoice:(Voice*)voice withAmp:(Float64)amp
{
  if (voice.isOn) {
    /* set up arguments needed by AudioFileReadPackets */
    UInt32 ioNumPackets = num_buf_samples;
    SInt64 inStartingPacket = (SInt64)filePos; /* convert float to int */
    UInt32 outNumBytes = 0;
  
    /* read some data */
    OSStatus result = AudioFileReadPackets(fileID,NO,&outNumBytes,NULL,inStartingPacket,&ioNumPackets,fileBuffer);
    if (result != noErr)
      NSLog(@"AudioFileReadPackets exception %d",(int)result);
    
    // ioNumPackets might change based on the the file pos (e.g. might be less than num_buf_samples because we've run out of samples since we're near the end of the file)
    if (repeat && ioNumPackets < num_buf_samples) {
      filePos = 0;
    } else {
      /* advance the member variable filePos to know where to read from next time this method is called */
      if (ioNumPackets > 0) {
        filePos += ioNumPackets;
      } else {
        [voice off];
        [(Voice_SF*)voice resetVoice];
      }
    }
    
    /* convert the buffer of sample read from sound file into the app's internal audio buffer */
    for (UInt32 buf_pos = 0; buf_pos < num_buf_samples; buf_pos++)
    {
      Float64 s = (SInt16)CFSwapInt16BigToHost(fileBuffer[buf_pos]);
      buffer[buf_pos] += amp * s / (SInt16)INT16_MAX;
    }
  }
}

@end
